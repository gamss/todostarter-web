import React, { FC, useState } from 'react';
import { css } from 'emotion';
import CheckBox from '../../atoms/CheckBox/CheckBox';
import Label from '../../atoms/Label/Label';
import { colors } from '../../../styles/colors';

const styles = {
  taskItem: css`
    height: fit-content;
    width: 100%;
    background: none;
    border: none;
    border-bottom: 1px solid ${colors.silverLight};
    padding: 10px 0;
    display: flex;
    cursor: pointer;
  `,
  checkBox: css`
    margin-left: 10px;
  `,
  title: css`
    align-items: center;
    margin-left: 10px;
    font-family: sans-serif;
    font-size: 15px;
    color: ${colors.blackBase};
  `,
};

export interface Task {
  id: string;
  title: string;
}

type Props = {
  task: Task;
};

const TaskItem: FC<Props> = ({ task: { title } }) => {
  const [itemClickable, setItemClickable] = useState<boolean>(true);

  const onMouseEnterCheckbox = (): void => {
    setItemClickable(false);
  };

  const onMouseLeaveCheckbox = (): void => {
    setItemClickable(true);
  };

  const onCompleteTask = (): void => {
    // dispatch complete task
  };

  return (
    <button disabled={!itemClickable} className={styles.taskItem}>
      <span
        onMouseEnter={onMouseEnterCheckbox}
        onMouseLeave={onMouseLeaveCheckbox}>
        <CheckBox onClick={onCompleteTask} className={styles.checkBox} />
      </span>
      <Label className={styles.title}>{title}</Label>
    </button>
  );
};

export default TaskItem;
