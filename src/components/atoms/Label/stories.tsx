import * as React from 'react';
import { Meta } from '@storybook/react/types-6-0';
import { css } from 'emotion';
import { colors } from '../../../styles/colors';
import Label from './Label';

const styles = {
  sampleStyle: css`
    font-family: sans-serif;
    font-size: 20px;
    color: #${colors.spaceCadetDark};
  `,
};

export default {
  title: 'atoms/Label',
} as Meta;

export const defaultLabel = () => <Label>Default Label</Label>;

export const styledLabel = () => (
  <Label className={styles.sampleStyle}>Styled Label</Label>
);
