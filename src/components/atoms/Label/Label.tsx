import React, { FC } from 'react';
import { css, cx } from 'emotion';

const styles = {
  default: css`
    font-style: normal;
    font-weight: 500;
    font-size: 15px;
    color: #555555;
  `,
};

type Props = {
  children: React.ReactNode;
  className?: string;
  style?: Object;
};

const Label: FC<Props> = props => {
  const className = cx(styles.default, props.className);
  return (
    <span className={className} style={props.style}>
      {props.children}
    </span>
  );
};

export default Label;
