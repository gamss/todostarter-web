import * as React from 'react';
import { Meta } from '@storybook/react/types-6-0';
import TaskItem from './TaskItem';

export default {
  title: 'molecules/TaskItem',
  excludeStories: /.*Data$/,
} as Meta;

export const taskData = {
  id: '1',
  title: 'Test Task',
};

export const defaultTaskItem = () => <TaskItem task={taskData} />;
