import * as React from 'react';
import { withKnobs, object } from '@storybook/addon-knobs';
import { Plus, Burger, Check } from './';

export default {
  title: 'atoms/Icons',
  decorators: [withKnobs],
};

const styleObject = {
  fontSize: '26px',
  color: '#000',
  margin: '0 8px',
};

export const defaultIcons = () => (
  <>
    <Plus style={object('Style', styleObject)} />
    <Burger style={object('Style', styleObject)} />
    <Check style={object('Style', styleObject)} />
  </>
);
