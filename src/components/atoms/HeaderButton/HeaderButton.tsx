import React, { FC } from 'react';
import { css, cx } from 'emotion';

const styles = {
  button: css`
    background: none;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    display: grid;
    place-content: center;
    height: 35px;
    width: 35px;
    &:hover {
      background: rgb(224, 224, 224, 0.3);
    }
  `,
};

type Props = Pick<
  React.ButtonHTMLAttributes<HTMLButtonElement>,
  'onClick' | 'className'
>;

const HeaderButton: FC<Props> = props => {
  const className = cx(styles.button, props.className);
  return (
    <button className={className} onClick={props.onClick}>
      {props.children}
    </button>
  );
};

export default HeaderButton;
