import * as React from 'react';
import { FC } from 'react';
import Icon from '@ant-design/icons';

type Props = {
  [key: string]: string | number | Function | object;
};

const Plus: FC<Props> = props => {
  return (
    <Icon
      {...props}
      component={() => (
        <svg
          fill="currentColor"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24"
          width="1em"
          height="1em">
          <path
            fill-rule="evenodd"
            d="M22 10.996L13.003 10.996 13.003 2 10.997 2 10.997 10.996 2 10.996 2 13.003 10.997 13.003 10.997 22 13.003 22 13.003 13.003 22 13.003z"
            clip-rule="evenodd"
          />
        </svg>
      )}
    />
  );
};

export default Plus;
