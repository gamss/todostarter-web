import * as React from 'react';
import { Meta } from '@storybook/react/types-6-0';
import { action } from '@storybook/addon-actions';
import CheckBox from './CheckBox';

export default {
  title: 'atoms/Checkbox',
  excludeStories: /.*Data$/,
} as Meta;

const actionsData = {
  onClick: action('onClick'),
};

export const defaultCheckbox = () => <CheckBox {...actionsData} />;
