import React, { FC } from 'react';
import { css } from 'emotion';
import Label from '../../atoms/Label/Label';
import TaskItem, { Task } from '../../molecules/TaskItem/TaskItem';
import { colors } from '../../../styles/colors';

const styles = {
  titleContainer: css`
    padding: 10px;
  `,
  title: css`
    font-family: sans-serif;
    font-size: 20px;
    color: ${colors.blackBase};
  `,
};

interface List {
  id: string;
  title: string;
  tasks: Array<Task>;
}

type Props = {
  list: List;
};

const TaskList: FC<Props> = ({ list: { title, tasks } }) => {
  return (
    <div>
      <div className={styles.titleContainer}>
        <Label className={styles.title}>{title}</Label>
      </div>
      <div>
        {tasks.map(task => (
          <TaskItem task={task} />
        ))}
      </div>
    </div>
  );
};

export default TaskList;
