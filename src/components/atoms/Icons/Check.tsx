import * as React from 'react';
import { FC } from 'react';
import { css } from 'emotion';
import Icon from '@ant-design/icons';

type Props = {
  [key: string]: string | number | Function | object;
};

const Check: FC<Props> = props => {
  const styles = {
    polyline: css`
      fill: none;
      stroke: currentColor;
      stroke-linecap: round;
      stroke-linejoin: round;
      stroke-width: 32px;
    `,
  };

  return (
    <Icon
      {...props}
      component={() => (
        <svg
          fill="currentColor"
          xmlns="http://www.w3.org/2000/svg"
          width="1em"
          height="1em"
          viewBox="0 0 512 512">
          <title>ionicons-v5-e</title>
          <polyline
            points="416 128 192 384 96 288"
            className={styles.polyline}
          />
        </svg>
      )}
    />
  );
};

export default Check;
