import * as React from 'react';
import { FC } from 'react';
import Icon from '@ant-design/icons';

type Props = {
  [key: string]: string | number | Function | object;
};

const Burger: FC<Props> = props => {
  return (
    <Icon
      {...props}
      component={() => (
        <svg
          fill="currentColor"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24"
          width="1em"
          height="1em">
          <path d="M0 11H24V13H0zM0 2H24V4H0zM0 20H24V22H0z" />
        </svg>
      )}
    />
  );
};

export default Burger;
