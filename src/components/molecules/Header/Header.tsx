import React, { FC } from 'react';
import { css } from 'emotion';
import { colors } from '../../../styles/colors';
import { Plus, Burger } from '../../atoms/Icons/';
import HeaderButton from '../../atoms/HeaderButton/HeaderButton';
import Label from '../../atoms/Label/Label';

const styles = {
  header: css`
    height: fit-content;
    width: 100%;
    display: flex;
    background: ${colors.spaceCadetBase};
    align-items: center;
    justify-content: space-between;
    padding: 10px 0;
  `,
  icon: css`
    font-size: 16px;
    color: ${colors.whiteBase};
  `,
  burger: css`
    margin-left: 20px;
  `,
  plus: css`
    margin-right: 20px;
  `,
  label: css`
    font-family: sans-serif;
    font-size: 20px;
    color: ${colors.whiteBase};
  `,
};

const Header: FC = () => {
  return (
    <div className={styles.header}>
      <HeaderButton className={styles.burger}>
        <Burger className={styles.icon} />
      </HeaderButton>
      <Label className={styles.label}>TodoStarter</Label>
      <HeaderButton className={styles.plus}>
        <Plus className={styles.icon} />
      </HeaderButton>
    </div>
  );
};

export default Header;
