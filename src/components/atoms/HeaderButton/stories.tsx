import * as React from 'react';
import { Meta } from '@storybook/react/types-6-0';
import { css } from 'emotion';
import { colors } from '../../../styles/colors';
import HeaderButton from './HeaderButton';
import { Plus, Burger } from '../Icons';

const styles = {
  decorator: css`
    background: ${colors.spaceCadetBase};
    display: flex;
    height: fit-content;
    width: fit-content;
    padding: 10px;
  `,
  button: css`
    margin: 0 5px;
  `,
  icon: css`
    font-size: 16px;
    color: #fff;
  `,
};

export default {
  title: 'atoms/HeaderButtons',
  decorators: [
    Story => (
      <div className={styles.decorator}>
        <Story />
      </div>
    ),
  ],
} as Meta;

export const defaultHeaderButtons = () => (
  <>
    <HeaderButton className={styles.button}>
      <Plus className={styles.icon} />
    </HeaderButton>
    <HeaderButton className={styles.button}>
      <Burger className={styles.icon} />
    </HeaderButton>
  </>
);
