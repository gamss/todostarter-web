import * as React from 'react';
import { Meta } from '@storybook/react/types-6-0';
import Header from './Header';

export default {
  title: 'molecules/Header',
} as Meta;

export const defaultHeader = () => <Header />;
