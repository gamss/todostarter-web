import * as React from 'react';
import { Meta } from '@storybook/react/types-6-0';
import TaskList from './TaskList';

export default {
  title: 'organisms/TaskList',
  excludeStories: /.*Data$/,
} as Meta;

export const listData = {
  id: '123',
  title: 'Grocery List',
  tasks: [
    {
      id: 'sdf0',
      title: 'fruits',
    },
    {
      id: 'ms0sa',
      title: 'meat',
    },
    {
      id: '1adas',
      title: 'drinks',
    },
  ],
};

export const defaultTaskList = () => <TaskList list={listData} />;
