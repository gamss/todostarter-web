import React, { FC, useState } from 'react';
import { css, cx } from 'emotion';
import { Check } from '../Icons';
import { colors } from '../../../styles/colors';

const styles = {
  box: css`
    width: 20px;
    height: 20px;
    display: grid;
    place-content: center;
    border: 1px solid ${colors.silverDarker};
    cursor: pointer;
  `,
  boxDefault: css`
    background: none;
    &:hover [data-comp='checkIcon'] {
      color: ${colors.blackBase};
    }
  `,
  boxChecked: css`
    background: ${colors.silverDarker};
  `,
  check: css`
    font-size: 18px;
    color: ${colors.whiteBase};
  `,
};

type Props = {
  onClick: () => void;
  className?: string;
};

const CheckBox: FC<Props> = props => {
  const [checked, setChecked] = useState<boolean>(false);

  const onClick = (): void => {
    if (!checked) {
      setChecked(true);
      props.onClick();
    }
  };

  const boxClassName = cx(
    styles.box,
    checked ? styles.boxChecked : styles.boxDefault,
    props.className,
  );

  return (
    <button className={boxClassName} onClick={onClick}>
      <Check data-comp="checkIcon" className={styles.check} />
    </button>
  );
};

export default CheckBox;
